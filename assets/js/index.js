   $(document).ready(function () {

       var $animateElements = $(".animateDiv");
       var $window = $(window);

       function scrollAnimate() {
           var windowHeight = $window.height();
           var windowTopPosition = $window.scrollTop();
           var windowBottomPosition = ($window.scrollTop() + windowHeight);
           if ($(this).scrollTop() > 140) {
               $('.scrollToTop').fadeIn();
           } else {
               $('.scrollToTop').fadeOut();
           }
           $.each($animateElements, function () {
               var $element = $(this);
               var elementTopPosition = $element.offset().top;
               var elementHeight = $element.innerHeight();
               var elementBottomPosition = (elementTopPosition + elementHeight);
               if (elementTopPosition >= windowBottomPosition) {
                   $element.addClass('fadeInLeft');
               }
           });
       }
       $window.on('scroll resize', scrollAnimate);
       //    $window.trigger('scroll');


       //Click event to scroll to top
       $('.scrollToTop').click(function () {
           $('html, body').animate({ scrollTop: 0 }, 800);
           return false;
       });
   });
