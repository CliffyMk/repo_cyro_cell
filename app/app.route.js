/*global angular*/
(function () {
    'use strict';
    angular.module("CyroCellApp")
        .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
            $stateProvider.state('main', {
                    url: '/main',
                    templateUrl: 'app/components/main/main.html'
                })
                .state('powerofstemcells', {
                    url: '/powerofstemcells',
                    templateUrl: 'app/components/aboutStemcells/uniquePower.html'
                })
                .state('benefits', {
                    url: '/benefits',
                    templateUrl: 'app/components/aboutStemcells/benefits.html'
                })
                .state('treatable', {
                    url: '/treatable',
                    templateUrl: 'app/components/aboutStemcells/treatableDisease.html'
                })
                .state('stemcellBanking', {
                    url: '/stemcellBanking',
                    templateUrl: 'app/components/aboutStemcells/stemcellsBanking.html'
                })
                .state('aboutUs', {
                    url: '/aboutUs',
                    templateUrl: 'app/components/aboutUs/aboutUs.html'
                })
                .state('services', {
                    url: '/services',
                    templateUrl: 'app/components/services/services.html'
                })
                .state('contactUs', {
                    url: '/contactUs',
                    templateUrl: 'app/components/contactUs/contactUs.html'
                })
                .state('pricing', {
                    url: '/pricing',
                    templateUrl: 'app/components/planAndPricing/pricing/pricing.html'
                })
                .state('additonalService', {
                    url: '/additonalService',
                    templateUrl: 'app/components/services/additionalService.html'
                })
                .state('boneMarrow', {
                    url: '/boneMarrow',
                    templateUrl: 'app/components/services/boneMarrow.html'
                })
                .state('cordBlood', {
                    url: '/cordBlood',
                    templateUrl: 'app/components/services/cordBlood.html'
                })
                .state('dendriticCell', {
                    url: '/dendriticCell',
                    templateUrl: 'app/components/services/dendriticCell.html'
                })
                .state('eyeLenticule', {
                    url: '/eyeLenticule',
                    templateUrl: 'app/components/services/eyeLenticule.html'
                })
                .state('peripheralBlood', {
                    url: '/peripheralBlood',
                    templateUrl: 'app/components/services/peripheralBlood.html'
                });
            $urlRouterProvider.otherwise('/main');
        }]);
}());
